var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.075
var dimension = 11
var offset = 0.00
var array = create2DArray(dimension, dimension, 0, false)
var rand = []
var numOfShape = 51
var limit = 0.666

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  for (var i = 0; i < numOfShape; i++) {
    rand.push([Math.floor(Math.random() * (dimension - 1)), Math.floor(Math.random() * (dimension - 1))])
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < dimension - 1; i++) {
    for (var j = 0; j < dimension; j++) {
      push()
      translate(0, 0)
      noFill()
      stroke(255)
      strokeWeight(boardSize * 0.005)
      strokeCap(ROUND)
      line(
        windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize + array[i][j][0] * boardSize * initSize,
        windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize + array[i][j][1] * boardSize * initSize,
        windowWidth * 0.5 + ((i + 1) - Math.floor(dimension * 0.5)) * boardSize * initSize + array[i + 1][j][0] * boardSize * initSize,
        windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize + array[i + 1][j][1] * boardSize * initSize
      )
      pop()
    }
  }

  for (var i = 0; i < dimension; i++) {
    for (var j = 0; j < dimension - 1; j++) {
      push()
      translate(0, 0)
      noFill()
      stroke(255)
      strokeWeight(boardSize * 0.005)
      strokeCap(ROUND)
      line(
        windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize + array[i][j][0] * boardSize * initSize,
        windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize + array[i][j][1] * boardSize * initSize,
        windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize + array[i][j + 1][0] * boardSize * initSize,
        windowHeight * 0.5 + ((j + 1) - Math.floor(dimension * 0.5)) * boardSize * initSize + array[i][j + 1][1] * boardSize * initSize
      )
      pop()
    }
  }

  for (var i = 0; i < rand.length; i++) {
    fill(255)
    stroke(255)
    strokeWeight(boardSize * 0.005)
    strokeCap(ROUND)
    beginShape()
    vertex(windowWidth * 0.5 + (rand[i][0] - Math.floor(dimension * 0.5)) * boardSize * initSize + array[rand[i][0]][rand[i][1]][0] * boardSize * initSize,
    windowHeight * 0.5 + (rand[i][1] - Math.floor(dimension * 0.5)) * boardSize * initSize + array[rand[i][0]][rand[i][1]][1] * boardSize * initSize)
    vertex(windowWidth * 0.5 + ((rand[i][0] + 1) - Math.floor(dimension * 0.5)) * boardSize * initSize + array[rand[i][0] + 1][rand[i][1]][0] * boardSize * initSize,
    windowHeight * 0.5 + (rand[i][1] - Math.floor(dimension * 0.5)) * boardSize * initSize + array[rand[i][0] + 1][rand[i][1]][1] * boardSize * initSize)
    vertex(windowWidth * 0.5 + ((rand[i][0] + 1) - Math.floor(dimension * 0.5)) * boardSize * initSize + array[(rand[i][0] + 1)][(rand[i][1] + 1)][0] * boardSize * initSize,
    windowHeight * 0.5 + ((rand[i][1] + 1) - Math.floor(dimension * 0.5)) * boardSize * initSize + array[(rand[i][0] + 1)][(rand[i][1] + 1)][1] * boardSize * initSize)
    vertex(windowWidth * 0.5 + (rand[i][0] - Math.floor(dimension * 0.5)) * boardSize * initSize + array[rand[i][0]][rand[i][1] + 1][0] * boardSize * initSize,
    windowHeight * 0.5 + ((rand[i][1] + 1) - Math.floor(dimension * 0.5)) * boardSize * initSize + array[rand[i][0]][rand[i][1] + 1][1] * boardSize * initSize)
    endShape(CLOSE)
  }

  if (abs(sin(frameCount * 0.005)) > limit) {
    for (var i = 0; i < numOfShape * (abs(sin(frameCount * 0.005)) - limit); i++) {
      rand[i] = [Math.floor(Math.random() * (dimension - 1)), Math.floor(Math.random() * (dimension - 1))]
    }
  }
  offset = abs(sin(frameCount * 0.005) * 0.5)
  array = create2DArray(dimension, dimension, 0, false)
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = [-offset + Math.random() * offset * 2, -offset + Math.random() * offset * 2]
      }
    }
    array[i] = columns
  }
  return array
}
